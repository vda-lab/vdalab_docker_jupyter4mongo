# bind STUDENT to the first argument

MONGO_SERVER='vdalab-docker-mongo-35bb8a5c-1.1d9f54c9.cont.dockerapp.io'

STUDENT=$1

# create student directory
mkdir $STUDENT

# copy notebook template to student folder
cp pymongo_exercises.ipynb $STUDENT

# replace template placeholders with student name
sed -i -e s/STUDENT_NAME/$STUDENT/g $STUDENT/pymongo_exercises.ipynb

# replace template placeholders with mongo server url
sed -i -e s/MONGO_SERVER/$MONGO_SERVER/g $STUDENT/pymongo_exercises.ipynb